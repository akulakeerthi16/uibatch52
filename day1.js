console.log("Hello World");


//var : it is used for global and functional scope
//let : is a block scoped
//const : is a block scoped

if(true){
    let a=20;
    console.log(a);
}

//Data Types
let name = "JavaScript";
let id = 101;
let coursecompletion = true;
let job = null;
let batch;
console.log(typeof(name));
console.log(typeof(id));
console.log(typeof(coursecompletion));
console.log(typeof(job));
console.log(typeof(batch));

//Complex data types
let scores = [34,"hello",90,89,34]
console.log(scores[1]);
console.log(scores[4]);
console.log(scores.length);

//looping statements for loop, do while
for(let i=0;i<scores.length;i++){
    console.log(scores[i]);
}

//Functions---block of reusable code

function addition(){
    let result = 30+40;

    return result;
    
}  //function declaration

let additionval = addition() //function calling
console.log(additionval);

function subtraction(num1,num2){
    return num1-num2;
}
console.log(subtraction(90,40));



//create a function that should accept two values from the user and print the max value

function maxVal(a,b){
    return Math.max(a,b);
//alert("The max value is"+maxVal);

}
console.log(maxVal(190,180));

//Find a min value between 3 values 

function minval(a,b,c){
    return Math.min(a,b,c);
}
console.log(minval(10,25,2));

//Generate random number

let randomNumber = Math.round(Math.random()*60);
console.log(randomNumber);

console.log(Math.ceil(56.1));
console.log(Math.abs(-45));
console.log(Math.pow(2,5));
console.log(Math.sqrt(2));

//find the even number,odd number between 1 to 100
//print even numbers and odd numbers between 1 to 50

let i 
let a = 100
console.log("All even numbers from 1 to 100")
for (i=1;i<=a;i++)
{
  if(i%2==0){
    console.log(i)
  }
}




console.log('Odd numbers are');
for (var j = 1 ; j < 50 ; j += 2 ) {
   console.log(j);
}


console.log('Even numbers are ');
for (var j = 2 ; j < 50 ; j += 2 ) {
  console.log(j);
}


//git
//git init --initislizing git
//ls --to check the files
//ls -a -- to check hidden files
//git config --global user.name "KEERTHI A"
//git config --global user.email "akulakeerthi16@gmail.com"
//git add .
//git commit -m "give any message"
//git status
//git remote add origin <url>
//git branch
//git push --set-upstream origin branchname





function findEvenandOdd(num1){
    if(num1%2==0){
        console.log(num1 +"is a even number")
    }else{
        console.log(num1 +"is a odd number")
    }
}